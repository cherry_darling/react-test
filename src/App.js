 import React from 'react';
 import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
 import axios from 'axios';
 import ReactHtmlParser, { processNodes, convertNodeToElement } from 'react-html-parser';

 const options = {
  "xmlMode": true
};

class Banner extends React.Component {
  render (){
    return (
      <div className="banner">
        <a href="/state/index.html"><img className="logo" src="/logo.png" alt="UI Extension Wordmark" /></a>
      </div>
    )
  }
}

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menus: {},
      uuids: {},
      nids: {},
    }
  }
  render() {
     var uuid = '';
     var nid = '';
     var url = '';
     var params = { location: 'state', page: ''};

     if( this.props.match ){
       params = this.props.match.params; 
       url = this.props.match.url;
       uuid = this.state.uuids[ '/' + params.location + '/' + params.page ];
       nid = this.state.nids[ '/' + params.location + '/' + params.page ];
     }

     var menu = this.state.menus[ params.location ];
     if(menu == null){
       menu = [];
       axios.get("//dev-ichabod.pantheonsite.io/api/menu_items/" + params.location + "?_format=json")
       .then((response) => {
         console.log(response);
         var menus = this.state.menus;
         menus[ params.location ] = response.data;
         this.setState( { menus: menus } );
         var uuids = this.state.uuids;
         var nids = this.state.nids;
         response.data.map( (item) => {
           uuids[ item.relative ] = item.uuid;
           nids[ item.relative ] = item.uri;
         });
       })
       .catch( (error) =>{
       });
     }

     return (
      <div>
      <nav className="main-menu">
        <ul>
          { menu.map( (item) => <li key={item.key} className="menu-item"><Link to={item.relative}>{item.title}</Link></li> )}
        </ul>
      </nav>
         <MultiContent nid={nid} url={url} />

         {/*// UNCOMMENT THIS TO REQUEST SINGLE NODE <SingleContent nid={nid} />*/}
         <div class="alert">
         <h4>Location: {params.location} </h4>
         <h4>Page: {params.page} </h4>
         <p><a href="/jsw/cui-jumentum-quibus.html">Test JSW LINK</a></p>
         </div>
      </div>
     )
  }
}

class MultiContent extends React.Component {
  constructor(props){
    super(props);
    this.state = { nodes: {} };
  }
  componentWillMount(){
    axios.get("//dev-ichabod.pantheonsite.io/api/nodes?_format=json")
    .then( (response) => {
      console.log(response);
      var nodes = this.state.nodes;
      response.data.map( (item) => {
        // map by path alias so we have something to match on
        nodes[ item.path[0].alias ] = item;
      });
      this.setState( { nodes: nodes } );
      console.log(nodes);
    })
    .catch( (error) => {
    });
  }
  render(){
    var node = this.state.nodes[ this.props.url ];
    console.log(this.props.url);
    console.log(node);
    var title = '';
    var body = '';
    if(node != null){
      title = node.title[0].value;
      body = node.body[0].value;
    }
    return (
        <div className="row"><div class="col-sm-8">
        <h1>{title}</h1>
        { ReactHtmlParser(body, {transform: HtmlTransform}) }
        </div></div>
    )
  }
}

const HtmlTransform = (node) => {
  // wrap polygon tag with class matching "County .*" in a React link
  if( node.type === 'tag' && node.name === 'polygon'){
    var node_class = node.attribs.class;
    if(/County .*/.test(node_class)){
      var county = node.attribs.class.replace('County ','').toLowerCase();
      return (
        <Link to={ '/' + county }>
          { convertNodeToElement(node) }
        </Link>
      );
    }
  }
  if( node.type === 'tag' && node.name === 'svg'){
     node.attribs['viewbox'] = node.attribs['viewBox'];
     delete node.attribs['viewbox'];
  }
}

//  UNCOMMENT TO REQUEST NODES ONE AT A TIME -- it won't handle requests from clicking a link in body content so that will need to be built
// class SingleContent extends React.Component {
//   constructor(props){
//     super(props);
//     this.state = { title: "", body: "", nid: "" };
//   }
//   render(){
//     var body = this.state.body;
//     var title = this.state.title;
//     if(this.props.nid != this.state.nid){
//        body = '';
//        title = '';
//        axios.get("//dev-ichabod.pantheonsite.io/" + this.props.nid  + "?_format=json")
//        .then( (response) => {
//          console.log(response);
//          this.setState( { nid: this.props.nid, body: response.data.body[0].value, title: response.data.title[0].value } );
//        })
//        .catch( (error) => {
//        });
//     }
//     return (
//         <div><h1>SINGLE NODE REQUEST: {title}</h1>{body}</div>
//     )
//   }
// }

class App extends React.Component {
  render() {
   return (
     <Router>
      <Route path="/:location/:page" children={ ({match}) => (
         <div className="container">
             <Banner /> 
             <Menu match={match} /> 
         </div>
       )}/>
     </Router>
    );
  }
}
 
export default App;